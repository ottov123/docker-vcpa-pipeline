#!/usr/bin/env python2.7
from __future__ import print_function

from subprocess import check_call
import argparse
import shlex
import json
import os
import boto3
from common_utils.s3_utils import download_file,upload_folder,file_exists
from common_utils.genomicsdb import exportSession
import run_vcf2tiledb as genDB

GATK='/GenomeAnalysisTK-3.7.jar'

#java -jar /GenomeAnalysisTK-3.7.jar -T GenotypeGVCFs -R /GRCh38_full_analysis_set_plus_decoy_hla.fa --dbsnp /dbsnp_137.sorted.hg19_no_chr.vcf.gz --variant CHR1.part0.g.vcf.gz -L chr1 -o out.g.vcf.gz

WORKDIR = genDB.WORKDIR
PVCFDIR = genDB.PVCFDIR

def main():
    parser = argparse.ArgumentParser(conflict_handler='resolve')
    parser.add_argument('--gatk_program', type=str, required=True)
    parser.add_argument('--gatk_ref', type=str, required=True)
    parser.add_argument('--gatk_dbsnp', type=str, required=True)

    parser.add_argument('--gatk_args', type=str, help='Additional GATK Arguments', required=False, default=None, nargs='*', action='store')

    args, extr = parser.parse_known_args()
    print(args)

    argparser = genDB.parse4vcf2tiledb(parser)
    args2, extr = argparser.parse_known_args()

#Namespace(callset_s3_path='s3://wanglab-play/genomicsdb-ecs/callsets-s3.chr1.json', chr='chr1', index=114,
#loader_s3_path='s3://wanglab-play/genomicsdb-ecs/loader.200.chr1.json', results_s3_path='s3://wanglab-play/genomicsdb-ecs/results-kamboh/', vid_s3_path='s3://wanglab-play/genomicsdb-ecs/hg38-vid.json')

    if args2.index == None:
      idx = int(os.getenv('AWS_BATCH_JOB_ARRAY_INDEX'))
    else:
      idx = args2.index

    outVcf = '{}-{}.genotypedGVCF.g.vcf.gz'.format(args2.chr, idx)
    s3_destination = args2.results_s3_path.rstrip('/') + '/' + outVcf

    if not file_exists(s3_destination):
        os.environ['SKIP_UPLOAD'] = '1'
        genDB.main()
    else:
        print("File {} found, not running".format(s3_destination))
        return

    # Download reference genome, dbsnp, + indexes
    print("Downloading ref genome {} to {}".format(args.gatk_ref, WORKDIR))
    #refG = download_file(args.gatk_ref, WORKDIR)

    # download only the required chr from the ref sequence
    # change the filename to include contig
    refG = list(os.path.basename(args.gatk_ref).rpartition('.'))
    refG.insert(-1, args2.chr + '.')
    refG.insert(0, WORKDIR + '/')
    refG = ''.join(refG)
    cmd = 'samtools faidx -o {outfile} {s3path} {region}'.format(
                outfile  = refG
                ,s3path  = args.gatk_ref
                ,region =args2.chr
                )
    check_call(shlex.split(cmd)) # samtools 1.8

    print("Downloading ref genome index to {}".format(WORKDIR))
    #download_file(args.gatk_ref + '.fai', WORKDIR)
    check_call(shlex.split('samtools faidx {}'.format(refG) ))

    print("Downloading ref genome dict to {}".format(WORKDIR))
#    download_file(".".join(args.gatk_ref.split('.')[0:-1]) + '.dict', WORKDIR)
    check_call(shlex.split('samtools dict -o {} {}'.format(refG.rsplit('.',1)[0] + '.dict',refG) ))

    print("Downloading dbSNP VCF {} to {}".format(args.gatk_dbsnp, WORKDIR))
    dbsnp = download_file(args.gatk_dbsnp, WORKDIR)

    print("Downloading dbSNP VCF index to {}".format(WORKDIR))
    download_file(args.gatk_dbsnp + '.tbi', WORKDIR)

    # Get Offset
    vid_path = genDB.download_required_files(args2.vid_s3_path)[0]
    with open(vid_path) as vid_file:
        hg = json.load(vid_file)

    offset = hg['contigs'][args2.chr]['tiledb_column_offset']

    # Read-in loader
    loader_path = genDB.download_required_files(args2.loader_s3_path)[0]
    # Extract start/end for this partition
    with open(loader_path) as loader_file:
        ldr = json.load(loader_file)

    start = ldr['column_partitions'][idx]['begin'] - offset - 1
    end   = ldr['column_partitions'][idx]['end']   - offset + 1
    if start < 0: start = 1

    pos = "%s:%s-%s" % (args2.chr, start, end) # tabix region

    # Extract infile
    infile = ldr['column_partitions'][idx]['vcf_output_filename']
    outVcf = '{}/{}-{}.genotypedGVCF.g.vcf.gz'.format(PVCFDIR, args2.chr, idx)
    log    = '{}/{}-{}.genotypedGVCF.log'.format(PVCFDIR, args2.chr, idx)
    del ldr

    if args.gatk_args == None:
        gatk_args = ''
    else:
        gatk_args = ' '.join(map(lambda x : ' --' + x.replace("'",''), args.gatk_args))

    cmd = 'java -jar {} -T {} -R {} --dbsnp {} --variant {} -L {} --log_to_file {} -o {} {}'.format(
                        GATK,
                        args.gatk_program,
                        refG, dbsnp, infile,
                        pos, log, outVcf,
                        gatk_args)
    print(cmd)
    check_call(shlex.split(cmd))

    check_call(shlex.split('rm -f {infile} {infile}.tbi'.format(infile=infile)))

    print("Clear session")
    clearSession()

    print("Uploading to %s" % (args2.results_s3_path) )
    upload_folder(args2.results_s3_path, PVCFDIR)

    print ("Completed pipeline step")

def refreshSession():
    del os.environ['AWS_SESSION_TOKEN']
    exportSession()

def clearSession():
    if 'AWS_ACCESS_KEY_ID' in os.environ:
        del os.environ['AWS_ACCESS_KEY_ID']

    if 'AWS_SECRET_ACCESS_KEY' in os.environ:
        del os.environ['AWS_SECRET_ACCESS_KEY']

    if 'AWS_SESSION_TOKEN' in os.environ:
        del os.environ['AWS_SESSION_TOKEN']


if __name__ == '__main__':
    main()
